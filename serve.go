package main

import (
	// "io"

	"database/sql"

	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(CLogMiddleware)

	// Db Context Middleware
	db, err := sql.Open("sqlite3", "orders.db")
	if err != nil {
		log.Fatal(err)
	}
	err = InitCustomers(db)
	err = InitOrdersTable(db)
	if err != nil {
		log.Fatal("Init failed...")
	}
	e.Use(DbContextMiddleware(db))

	// Route attachments
	AttachCustomerHandlers(e)
	AttachOrderHandlers(e)

	log.Info("Listening on :3001...")
	e.Logger.Fatal(e.Start(":3001"))
}
