package main

import (
	"database/sql"
	"os"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
)

func CLogMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
			return err
		}

		logRequest(c.Request().URL.Path, c.Request().Method, c.Response().Status)

		return nil
	}
}

func logRequest(path string, method string, status int) {
	styles := log.DefaultStyles()
	color := 2
	if status >= 400 && status < 500 {
		color = 4
	}
	if status >= 500 {
		color = 5
	}
	if status >= 300 && status < 400 {
		color = 3
	}

	styles.Values["status"] = lipgloss.NewStyle().Foreground(lipgloss.ANSIColor(color))
	logger := log.New(os.Stdout)
	logger.SetStyles(styles)
	logger.Info("request", "method", method, "path", path, "status", status)
}

type DbContext struct {
	echo.Context
	db *sql.DB
}

func DbContextMiddleware(db *sql.DB) func(echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &DbContext{c, db}
			return next(cc)
		}
	}
}
