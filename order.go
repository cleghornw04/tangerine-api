package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
	_ "github.com/mattn/go-sqlite3"
)

func hCreateOrder(c echo.Context) error {
	var body struct {
		CustomerID int `json:"customer_id"`
	}
	if err := c.Bind(&body); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	db := c.(*DbContext).db

	var custCount int64
	err := db.QueryRow("SELECT COUNT(*) FROM customers WHERE id=?", body.CustomerID).
		Scan(&custCount)
	if err != nil {
		log.Error("err scanning row", "err", err)
		return c.NoContent(http.StatusInternalServerError)
	}

	if custCount < 1 {
		return c.String(
			http.StatusBadRequest,
			fmt.Sprintf("found %d customers with id %d", custCount, body.CustomerID),
		)
	}

	res, err := db.Exec("INSERT INTO orders VALUES(NULL, ?)", body.CustomerID)
	if err != nil {
		log.Error("error executing statement", "err", err)
		return c.NoContent(http.StatusInternalServerError)
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Error("error getting last insert id", "err", err)
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusCreated, map[string]any{"order_id": id, "customer_id": body.CustomerID})
}

func hGetOrder(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	ctx := c.(*DbContext)

	var data struct {
		ID         int64 `json:"order_id"`
		CustomerID int64 `json:"customer_id"`
	}
	err = ctx.db.QueryRow("SELECT * FROM orders WHERE id=?", int64(id)).
		Scan(&data.ID, &data.CustomerID)
	if err == sql.ErrNoRows {
		return c.String(http.StatusNotFound, "order not found")
	}

	return c.JSON(http.StatusOK, data)
}

func InitOrdersTable(db *sql.DB) error {
	log.Info("Creating order table in db...")
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS orders (
			ID INTEGER NOT NULL PRIMARY KEY,
			CustomerID INTEGER NOT NULL	
		);
	`)
	if err != nil {
		log.Error("error creating order table", "err", err)
	} else {
		log.Info("order table created succesfully")
	}
	return err
}

func AttachOrderHandlers(e *echo.Echo) {
	e.POST("/order", hCreateOrder)
	e.GET("/order/:id", hGetOrder)
}
