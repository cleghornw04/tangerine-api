package main

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/charmbracelet/log"
	"github.com/labstack/echo/v4"
)

func hCreateCustomer(c echo.Context) error {
	var body struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	}
	if err := c.Bind(&body); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	ctx := c.(*DbContext)

	res, err := ctx.db.Exec("INSERT INTO customers VALUES(NULL, ?, ?)", body.FirstName, body.LastName)
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, map[string]any{"id": id})
}

func hGetCustomer(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	ctx := c.(*DbContext)

	var data struct {
		ID        int64  `json:"customer_id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	}
	err = ctx.db.QueryRow("SELECT * FROM customers WHERE id=?", int64(id)).
		Scan(&data.ID, &data.FirstName, &data.LastName)
	if err == sql.ErrNoRows {
		return c.String(http.StatusNotFound, "customer not found")
	}

	return c.JSON(http.StatusOK, data)
}

func hGetCustomerCount(c echo.Context) error {
	ctx := c.(*DbContext)
	var count int64
	err := ctx.db.QueryRow("SELECT COUNT(*) FROM customers").Scan(&count)
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, map[string]any{"count": count})
}

func InitCustomers(db *sql.DB) error {
	log.Info("Creating user table...")
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS customers (
			ID INTEGER NOT NULL PRIMARY KEY,
			FirstName TEXT NOT NULL,
			LastName TEXT NOT NULL
		);
		`)
	if err != nil {
		log.Error("Failed to create user table", "err", err)
	} else {
		log.Info("Successfully created user table")
	}
	return err
}

func AttachCustomerHandlers(e *echo.Echo) {
	e.POST("/customer", hCreateCustomer)
	e.GET("/customer/:id", hGetCustomer)
	e.GET("/customers/count", hGetCustomerCount)
}
